<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Address;
class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alldata = Address::all();
        return  response()->json(['all'=>$alldata]);
        //return view('address',['address' => $alldata]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return view('address');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'first_address' => 'required',
            'second_address' => 'required',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'nick_name' => 'required|string|max:255',
            'telephone' => 'required|string|max:255',
            'fax' => 'required|string|max:255',
            'email' => 'required|string|max:255|email',
            'latitude' => 'required|string|max:255',
            'longtitude' => 'required|string|max:255',
        ]);
        $newAddress  = new Address();
        $newAddress->first_address = $request->input('first_address');
        $newAddress->second_address = $request->input('second_address');
        $newAddress->first_name = $request->input('first_name');
        $newAddress->last_name = $request->input('last_name');
        $newAddress->nick_name = $request->input('nick_name');
        $newAddress->telephone = $request->input('telephone');
        $newAddress->fax = $request->input('fax');
        $newAddress->email = $request->input('email');
        $newAddress->latitude = $request->input('latitude');
        $newAddress->longtitude = $request->input('longtitude');
        $newAddress->country_id = $request->input('country_id');
        $newAddress->province_id = $request->input('province_id');
        $newAddress->district_id = $request->input('district_id');
        $newAddress->subdistrict_id = $request->input('subdistrict_id');
        $newAddress->postcode_id = $request->input('postcode_id');
        $newAddress->save();
        return response()->json(['newinfo' => $newAddress],201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $find = Address::find($id);
        if($find){
            return response()->json(['find'=>$find]);
        }else{
            return response()->json(['message' => 'not found'],200);
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $find = Address::find($id);
        return  response()->json(['find'=>$find]);
        //return view('edit_address',['address' => $find]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $edit = Address::find($id);
        if(!$edit){
            return response()->json(['message' => 'Document not found'],404);   
        }
        $this->validate($request,[
            'first_address' => 'required',
            'second_address' => 'required',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'nick_name' => 'required|string|max:255',
            'telephone' => 'required|string|max:255',
            'fax' => 'required|string|max:255',
            'email' => 'required|string|max:255|email',
            'latitude' => 'required|string|max:255',
            'longtitude' => 'required|string|max:255',
        ]);
        $edit->first_address = $request->input('first_address');
        $edit->second_address = $request->input('second_address');
        $edit->first_name = $request->input('first_name');
        $edit->last_name = $request->input('last_name');
        $edit->nick_name = $request->input('nick_name');
        $edit->telephone = $request->input('telephone');
        $edit->fax = $request->input('fax');
        $edit->email = $request->input('email');
        $edit->latitude = $request->input('latitude');
        $edit->longtitude = $request->input('longtitude');
        $edit->country_id = $request->input('country_id');
        $edit->province_id = $request->input('province_id');
        $edit->district_id = $request->input('district_id');
        $edit->subdistrict_id = $request->input('subdistrict_id');
        $edit->postcode_id = $request->input('postcode_id');
        $edit->save();
        return response()->json(['updateinfo' => $edit],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Address::find($id);
        if($delete){
            $delete->delete();
            return response()->json(['message' => 'info deleted'],200);
        }else{
            return response()->json(['message' => 'not found'],200);
        }
        
      
    }
}
