<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('dim_addresses', function (Blueprint $table) {
             $table->increments('id');
             $table->longText('first_address');
             $table->longText('second_address');
             $table->string('first_name');
             $table->string('last_name');
             $table->string('nick_name');
             $table->string('telephone');
             $table->string('fax');
             $table->string('email');
             $table->text('latitude');
             $table->text('longtitude');
             //if you have table references you can use code after comment 
             $table->integer('country_id')->nullable(); // $table->foreign('country_id')->references('id')->on('country');
             $table->integer('province_id')->nullable(); // $table->foreign('province_id')->references('id')->on('province');
             $table->integer('district_id')->nullable(); // $table->foreign('district_id')->references('id')->on('district');
             $table->integer('subdistrict_id')->nullable(); // $table->foreign('subdistrict_id')->references('id')->on('subdistrict');
             $table->integer('postcode_id')->nullable(); // $table->foreign('postcode_id')->references('id')->on('postcode');
             $table->timestamps();
             $table->softDeletes();
         });
     }
 
     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('dim_addresses');
     }
}
