<?php

use Illuminate\Database\Seeder;

class AddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dim_addresses')->delete();
        $address_data = array(
            array(  
                'first_address' => '153 Logan Center',
                'second_address' => '7 Schurz Lane',
                'first_name' => 'Laskey',
                'last_name' => 'Marys',
                'nick_name' => 'Josiah',
                'telephone' => '+33 929 537 0786',
                'fax' => '+353-693-399-1460',
                'email' => 'jlaskey0@icq.com',
                'latitude' => '50.6140977',
                'longtitude' => '52.2509369',
                'country_id' => '1',
                'province_id' => '1',
                'district_id' => '1',
                'subdistrict_id' => '1',
                'postcode_id' => '1',
            ),
            array(
                'first_address' => '13608 Manitowish Alley',
                'second_address' => '564 Sullivan Avenue',
                'first_name' => 'Ram',
                'last_name' => 'McLenaghan',
                'nick_name' => 'Ari',
                'telephone' => '+33 929 537 0786',
                'fax' => '+353-693-399-1460',
                'email' => 'amclenaghan1@topsy.com',
                'latitude' => '50.6140977',
                'longtitude' => '52.2509369',
                'country_id' => '2',
                'province_id' => '2',
                'district_id' => '2',
                'subdistrict_id' => '2',
                'postcode_id' => '2',
            ),
            array(
                'first_address' => '8125 Cody Terrace',
                'second_address' => '8740 Magdeline Junction',
                'first_name' => 'Valdemar',
                'last_name' => 'Vigers',
                'nick_name' => 'Keefer',
                'telephone' => '+33 929 537 0786',
                'fax' => '+353-693-399-1460',
                'email' => 'kvigers2@indiegogo.com',
                'latitude' => '50.6140977',
                'longtitude' => '52.2509369',
                'country_id' => '3',
                'province_id' => '3',
                'district_id' => '3',
                'subdistrict_id' => '3',
                'postcode_id' => '3',
            ),
            array(
                'first_address' => '663 Glacier Hill Way',
                'second_address' => '4756 Banding Crossing',
                'first_name' => 'Drusi',
                'last_name' => 'Duddy',
                'nick_name' => 'Alfred',
                'telephone' => '+33 929 537 0786',
                'fax' => '+353-693-399-1460',
                'email' => 'aduddy3@forbes.com',
                'latitude' => '50.6140977',
                'longtitude' => '52.2509369',
                'country_id' => '4',
                'province_id' => '4',
                'district_id' => '4',
                'subdistrict_id' => '4',
                'postcode_id' => '4',
            ),
            array(
                'first_address' => '24 Marcy Court',
                'second_address' => '4297 Lukken Place',
                'first_name' => 'Renie',
                'last_name' => 'L\'Hommee',
                'nick_name' => 'Dex',
                'telephone' => '+33 929 537 0786',
                'fax' => '+353-693-399-1460',
                'email' => 'dlhommee4@weebly.com',
                'latitude' => '50.6140977',
                'longtitude' => '52.2509369',
                'country_id' => '5',
                'province_id' => '5',
                'district_id' => '5',
                'subdistrict_id' => '5',
                'postcode_id' => '5',
            ),
        );
        DB::table('dim_addresses')->insert($address_data);

    }
}
